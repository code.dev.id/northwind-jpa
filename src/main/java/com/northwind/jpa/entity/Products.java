/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.northwind.jpa.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author ahza0
 */
@Entity
@Table(name = "Products")
public class Products implements Serializable {

    @Id    
    @Column(name = "productid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer productID;

    @Column(name = "productname")
    @Size(max = 40)
    private String productName;
    
    @Column(name = "supplierid")
    private Integer supplierID;
    
    @Column(name = "categoryid")
    private Integer categoryID;
    
    @Column(name = "quantityperunit")
    @Size(max = 20)
    private String quantityPerUnit;
    
    @Column(name = "unitprice")
    private BigDecimal unitPrice;
    
    @Column(name = "unitsinstock")
    private Short unitsInStock;
    
    @Column(name = "unitsonorder")
    private Short UnitsOnOrder;
    
    @Column(name = "reorderlevel")
    private Short ReorderLevel;
    
    @Column(name = "discontinued")
    private Boolean Discontinued;
    
    
    public Integer getProductId() {
        return productID;
    }

    public void setProductId(Integer productId) {
        this.productID = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(Integer supplierID) {
        this.supplierID = supplierID;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public String getQuantityPerUnit() {
        return quantityPerUnit;
    }

    public void setQuantityPerUnit(String quantityPerUnit) {
        this.quantityPerUnit = quantityPerUnit;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Short getUnitsInStock() {
        return unitsInStock;
    }

    public void setUnitsInStock(Short unitsInStock) {
        this.unitsInStock = unitsInStock;
    }

    public Short getUnitsOnOrder() {
        return UnitsOnOrder;
    }

    public void setUnitsOnOrder(Short UnitsOnOrder) {
        this.UnitsOnOrder = UnitsOnOrder;
    }

    public Short getReorderLevel() {
        return ReorderLevel;
    }

    public void setReorderLevel(Short ReorderLevel) {
        this.ReorderLevel = ReorderLevel;
    }

    public Boolean getDiscontinued() {
        return Discontinued;
    }

    public void setDiscontinued(Boolean Discontinued) {
        this.Discontinued = Discontinued;
    }
    
    
}
